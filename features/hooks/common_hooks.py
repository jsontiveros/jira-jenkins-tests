"""Module for common hooks"""
import json
from http import HTTPStatus
from behave import fixture
from main.core.utils.json_manager import JsonManager
from main.core.requests_manager import RequestsManager
from main.core.utils.api_constants import HttpMethods as method
from main.core.utils.request_utils import RequestUtils as utils


@fixture
def delete_resource(context, endpoint):
    """Delete a resource found in endpoint

    :param context: Global context from behave
    :type context: obj
    :param endpoint: endpoint from which to delete resource
    :type endpoint: string
    """
    endpoint = utils.parse_id(context.id_dictionary, endpoint)
    RequestsManager.get_instance().do_request(method.DELETE.value, endpoint)


@fixture
def create_resource(context, endpoint, body_path, id_key=None):
    """Create a resource

    :param endpoint: endpoint to create a resource
    :type endpoint: string
    :param body: payload body to create a resource
    :type body: string
    :param id_dictionary: dictionary to save id of created resource
    :type id_dictionary: dictionary
    """
    body = json.dumps(JsonManager.read_json('payloads', body_path))
    endpoint = utils.parse_id(context.id_dictionary, endpoint)
    context.status_code, context.json_response = RequestsManager.get_instance().do_request(
        method.POST.value, endpoint, body)

    key = utils.get_id_keys(endpoint)[-1]
    if not id_key:
        id_key = "id"

    if context.status_code < HTTPStatus.BAD_REQUEST.value:
        context.id_dictionary[key] = context.json_response[id_key]
