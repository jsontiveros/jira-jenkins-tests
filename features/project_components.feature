@project_components
Feature: Project Components

    @get_project_components @fixture.create.project @fixture.create.project.component @fixture.delete.project
    Scenario: All project components are obtained
        Given the user defines a "GET" request to "/project/{projectId}/components"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_components/get_all_project_components.json"

    @get_project_components_paginated @fixture.create.project @fixture.create.project.component @fixture.delete.project
    Scenario: Project components paginated are obtained
        Given the user defines a "GET" request to "/project/{projectId}/component"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_components_paginated_project.json"
        And verifies response body with
            | key                               | value                    |
            | maxResults                        | "50"                     |
            | startAt                           | "0"                      |
            | total                             | "1"                      |
            | values                            | []                       |
            | values.componentBean.name         | ComponentTest            |
            | values.componentBean.description  | This is a Jira component |
            | values.componentBean.assigneeType | PROJECT_LEAD             |

    @get_project_component @fixture.create.project @fixture.create.project.component @fixture.delete.project
    Scenario: A project component is obtained
        Given the user defines a "GET" request to "/component/{componentId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_components/get_project_component.json"
        And verifies response body with
            | key              | value                    |
            | name             | ComponentTest            |
            | description      | This is a Jira component |
            | project          | TESS                     |
            | assigneeType     | PROJECT_LEAD             |
            | realAssigneeType | PROJECT_LEAD             |

    @create_project_component @fixture.create.project @fixture.delete.project
    Scenario: A project component is created
        Given the user defines a "POST" request to "/component"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test           |
            | description         | This is a Jira component |
            | project             | TESS                     |
            | assigneeType        | PROJECT_LEAD             |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 201
        And verifies the schema with "project_components/create_project_component.json"
        And verifies response body with
            | key              | value                    |
            | name             | Component Test           |
            | description      | This is a Jira component |
            | project          | TESS                     |
            | assigneeType     | PROJECT_LEAD             |
            | realAssigneeType | PROJECT_LEAD             |

    @update_project_component @fixture.create.project @fixture.create.project.component @fixture.delete.project
    Scenario: A project component is updated
        Given the user defines a "PUT" request to "/component/{componentId}"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test Update    |
            | description         | This is a Jira component |
            | project             | TESS                     |
            | assigneeType        | PROJECT_LEAD             |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_components/update_project_component.json"
        And verifies response body with
            | key              | value                    |
            | name             | Component Test Update    |
            | description      | This is a Jira component |
            | project          | TESS                     |
            | assigneeType     | PROJECT_LEAD             |
            | realAssigneeType | PROJECT_LEAD             |


    @delete_project_component @fixture.create.project @fixture.create.project.component
    Scenario: A project component is deleted
        Given the user defines a "DELETE" request to "/component/{componentId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And the user defines a "GET" request to "/component/{componentId}"
        And the user sends the request
        And the user verifies that the status code is 404

    ############## NEGATIVE TEST CASES #####################

    @get_project_components_invalid_id @negative
    Scenario Outline: All project components with invalid id or key are not obtained
        Given the user defines a "GET" request to "/project/<projectKey>/components"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                                  |
            | errors        | {}                                                     |
            | errorMessages | []                                                     |
            | errorMessages | No project could be found with <param> '<projectKey>'. |

        Examples: Project ids
            | projectKey | param |
            | 10030      | id    |
            | TESTT      | key   |

    @get_project_component_no_id @negative @fixture.create.project @fixture.delete.project
    Scenario Outline: A project component with invalid id is not obtained.
        Given the user defines a "GET" request to "/component/<id>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                      |
            | errors        | {}                                         |
            | errorMessages | []                                         |
            | errorMessages | The component with id <id> does not exist. |

        Examples: Component ids
            | id        |
            | 10030     |
            | invalidID |

    @create_project_component_no_project @negative
    Scenario Outline: A project component without project is not created
        Given the user defines a "POST" request to "/component"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test           |
            | description         | This is a Jira component |
            | project             | <projectKey>             |
            | assigneeType        | PROJECT_LEAD             |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                              |
            | errors        | {}                                                 |
            | errorMessages | []                                                 |
            | errorMessages | No project could be found with key '<projectKey>'. |

        Examples: Project ids
            | projectKey |
            | 10030      |
            | TESTT      |

    @create_project_component_invalid_id @negative
    Scenario: A project component without project id is not created
        Given the user defines a "POST" request to "/component"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test           |
            | description         | This is a Jira component |
            | assigneeType        | PROJECT_LEAD             |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 400
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                              |
            | errors        | {}                                                 |
            | errorMessages | []                                                 |
            | errorMessages | A project id must be specified for this operation. |

    @update_project_component_no_component @negative
    Scenario Outline: A project component with no component is not updated
        Given the user defines a "PUT" request to "/component/<id>"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test Update    |
            | description         | This is a Jira component |
            | project             | TESS                     |
            | assigneeType        | PROJECT_LEAD             |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                      |
            | errors        | {}                                         |
            | errorMessages | []                                         |
            | errorMessages | The component with id <id> does not exist. |

        Examples: Component ids
            | id     |
            | 10030  |
            | idword |

    @update_project_component_invalid_assigneeType @negative @fixture.create.project @fixture.create.project.component @fixture.delete.project
    Scenario: A project component with invalid assigneeType is not updated
        Given the user defines a "PUT" request to "/component/{componentId}"
            | key                 | value                    |
            | isAssigneeTypeValid | false                    |
            | name                | Component Test Update    |
            | description         | This is a Jira component |
            | project             | TESS                     |
            | assigneeType        | INVALID                  |
            | leadAccountId       | (accountId)              |
        When the user sends the request
        Then the user verifies that the status code is 400

    @delete_project_component_no_component @negative
    Scenario Outline: A project component with no component is not deleted
        Given the user defines a "DELETE" request to "/component/<id>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                      |
            | errors        | {}                                         |
            | errorMessages | []                                         |
            | errorMessages | The component with id <id> does not exist. |

        Examples: Component ids
            | id    |
            | 10030 |
