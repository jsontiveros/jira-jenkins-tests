@project_roles
Feature: Projects Roles

    @get_all_roles @smoke
    Scenario: All project roles are obtained
        Given the user defines a "GET" request to "/role"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/get_all_project_roles.json"

    @get_roles_project @fixture.create.project @fixture.delete.project
    Scenario: Project roles for project are obtained
        Given the user defines a "GET" request to "/project/{projectId}/role"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/get_role_project_roles.json"

    @get_roledetails_project @fixture.create.project @fixture.delete.project
    Scenario: Project role details for project are obtained
        Given the user defines a "GET" request to "/project/{projectId}/roledetails"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/get_roledetails_project_roles.json"

    @get_role_project @smoke @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role is obtained
        Given the user defines a "GET" request to "/role/{roleId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/get_project_role.json"
        And verifies response body with
            | key         | value                            |
            | name        | QA Manager                       |
            | description | Description of a QA Manager role |

    @create_project_role @smoke @fixture.delete.project.role
    Scenario: A project role is created
        Given the user defines a "POST" request to "/role"
            | key         | value                                         |
            | name        | QA Manager                                    |
            | description | A project role that represents QAs in project |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/create_project_role.json"
        And verifies response body with
            | key         | value                                         |
            | name        | QA Manager                                    |
            | description | A project role that represents QAs in project |

    @update_partially_project_role @smoke @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role is partially updated
        Given the user defines a "POST" request to "/role/{roleId}"
            | key  | value             |
            | name | QA Manager Update |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/update_project_role.json"
        And verifies response body with
            | key         | value                            |
            | name        | QA Manager Update                |
            | description | Description of a QA Manager role |


    @update_fully_project_role @smoke @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role is fully updated
        Given the user defines a "PUT" request to "/role/{roleId}"
            | key         | value                          |
            | name        | QA Manager                     |
            | description | Description QA Manager Updated |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "project_roles/update_project_role.json"
        And verifies response body with
            | key         | value                          |
            | name        | QA Manager                     |
            | description | Description QA Manager Updated |

    @delete_project_role @smoke @fixture.create.project.role
    Scenario: A project role is deleted
        Given the user defines a "DELETE" request to "/role/{roleId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And the user defines a "GET" request to "/role/{roleId}"
        And the user sends the request
        And the user verifies that the status code is 404

    ############## NEGATIVE TEST CASES #####################

    @get_roles_project_no_project @negative @fixture.create.project @fixture.delete.project
    Scenario Outline: All project roles with no project are not obtained
        Given the user defines a "GET" request to "/project/<projectKey>/role"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                                  |
            | errors        | {}                                                     |
            | errorMessages | []                                                     |
            | errorMessages | No project could be found with <param> '<projectKey>'. |

        Examples: Project ids
            | projectKey | param |
            | 10030      | id    |
            | TESTT      | key   |

    @get_roledetails_project_no_project @negative
    Scenario Outline: Project role details without project are not obtained
        Given the user defines a "GET" request to "/project/<projectKey>/roledetails"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                                  |
            | errors        | {}                                                     |
            | errorMessages | []                                                     |
            | errorMessages | No project could be found with <param> '<projectKey>'. |

        Examples: Project ids
            | projectKey | param |
            | 10030      | id    |
            | TESTT      | key   |

    @get_role_project_invalid_id @negative
    Scenario Outline: A project role with invalid id is not obtained
        Given the user defines a "GET" request to "/role/<id>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                         |
            | errors        | {}                            |
            | errorMessages | []                            |
            | errorMessages | Role with given id not found. |

        Examples: role ids
            | id    |
            | 10030 |

    @create_role_project_no_name @negative
    Scenario: A project role with no name is not created
        Given the user defines a "POST" request to "/role"
            | key         | value                                         |
            | description | A project role that represents QAs in project |
        When the user sends the request
        Then the user verifies that the status code is 400
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                 |
            | errors        | {}                                    |
            | errorMessages | []                                    |
            | errorMessages | Can not create a role without a name. |

    @create_project_role_same_name @negative @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role with same name is not created
        Given the user defines a "POST" request to "/role"
            | key         | value                                         |
            | name        | QA Manager                                    |
            | description | A project role that represents QAs in project |
        When the user sends the request
        Then the user verifies that the status code is 409
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key         | value                                                 |
            | errors      | {}                                                    |
            | errors.name | A project role with name 'QA Manager' already exists. |

    @update_partially_project_role_with_invalid_id @negative
    Scenario Outline: Update partially a project role with invalid id
        Given the user defines a "POST" request to "/role/<id>"
            | key  | value             |
            | name | QA Manager Update |
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                         |
            | errors        | {}                            |
            | errorMessages | []                            |
            | errorMessages | Role with given id not found. |

        Examples: role ids
            | id    |
            | 10030 |

    @update_partially_project_role_no_params @negative @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role with no params is not partially updated
        Given the user defines a "POST" request to "/role/{roleId}"
        When the user sends the request
        Then the user verifies that the status code is 400
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                      |
            | errors        | {}                                         |
            | errorMessages | []                                         |
            | errorMessages | Role name or description must be provided. |

    @update_fully_project_role_invalid_id @negative
    Scenario Outline: A project role with invalid id is not fully updated
        Given the user defines a "PUT" request to "/role/<id>"
            | key  | value             |
            | name | QA Manager Update |
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                         |
            | errors        | {}                            |
            | errorMessages | []                            |
            | errorMessages | Role with given id not found. |

        Examples: role ids
            | id    |
            | 10030 |

    @update_fully_project_role_no_params @negative @fixture.create.project.role @fixture.delete.project.role
    Scenario: A project role with no params is not partially updated
        Given the user defines a "PUT" request to "/role/{roleId}"
        When the user sends the request
        Then the user verifies that the status code is 400
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                       |
            | errors        | {}                                          |
            | errorMessages | []                                          |
            | errorMessages | Role name and description must be provided. |

    @delete_project_role @negative
    Scenario Outline: A project role with invalid id is not deleted
        Given the user defines a "DELETE" request to "/role/<id>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                         |
            | errors        | {}                            |
            | errorMessages | []                            |
            | errorMessages | Role with given id not found. |

        Examples: role ids
            | id    |
            | 10030 |


