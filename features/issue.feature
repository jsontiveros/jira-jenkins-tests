@issues
Feature: Issues

    @get_issue @smoke @fixture.create.issue @fixture.delete.issue
    Scenario: An issue is obtained
        Given the user defines a "GET" request to "/issue/{issueId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issues/get_issue.json"


    @create_issue @smoke @fixture.delete.issue
    Scenario Outline: An issue is created
        Given the user defines a "POST" request to "/issue"
            | key                   | value            |
            | fields                | {}               |
            | fields.issuetype      | {}               |
            | fields.issuetype.name | <issuetype>      |
            | fields.summary        | Issue issue      |
            | fields.description    | Desc Issue issue |
            | fields.project        | {}               |
            | fields.project.key    | TEST             |
        When the user sends the request
        Then the user verifies that the status code is 201
        And verifies the schema with "issues/create_issue.json"

        Examples: Issue types
            | issuetype |
            | Task      |
            | Story     |
            | Bug       |


    @update_issue @smoke @fixture.create.issue @fixture.delete.issue
    Scenario: An issue is updated
        Given the user defines a "PUT" request to "/issue/{issueId}"
            | key                | value                   |
            | fields             | {}                      |
            | fields.summary     | Update issue table      |
            | fields.description | description issue table |
        When the user sends the request
        Then the user verifies that the status code is 204


    @delete_issue @smoke @fixture.create.issue
    Scenario: An issue is deleted
        Given the user defines a "DELETE" request to "/issue/{issueId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And the user defines a "GET" request to "/issue/{issueId}"
        And the user sends the request
        And the user verifies that the status code is 404
