"""Module for implementation of BDD steps"""
import json
from http import HTTPStatus
from jsonschema import validate
from behave import step, use_step_matcher  # pylint: disable=E0611
from assertpy import assert_that
from main.core.utils import json_comparator
from main.core.utils.json_manager import JsonManager
from main.core.utils.request_utils import RequestUtils as utils
from main.jira.utils.request_utils import JiraRequestUtils as jira_utils
from main.core.utils.api_constants import HttpMethods as method

use_step_matcher("re")


@step(u'the user defines a "(?P<http_method>GET|POST|PUT|DELETE)" request to "(?P<endpoint>.*)"')
def step_retrieve_http_endpoint(context, http_method, endpoint):
    """Retrieve HTTP method and endpoint from Gherkin

    :param context: Global context from behave
    :type context: obj
    :param http_method: HTTP method
    :type http_method: string
    :param endpoint: application's endpoint
    :type endpoint: obj
    """

    context.endpoint = utils.parse_id(context.id_dictionary, endpoint)
    context.data_table = context.table
    context.http_method = http_method


use_step_matcher("parse")


@step(u"the user sends the request")
def step_impl_send(context):
    """Send HTTP request

    :param context: Global context from behave
    :type context: obj
    """
    if "json_body" not in context:
        context.json_body = json.dumps(jira_utils.generate_data(context.data_table))

    context.status_code, context.json_response = context.rm.do_request(context.http_method,
                                                                       context.endpoint,
                                                                       context.json_body)
    keys = utils.get_id_keys(context.endpoint)
    if context.http_method == method.POST.value\
            and context.status_code < HTTPStatus.BAD_REQUEST.value:
        context.id_dictionary[keys[-1]] = context.json_response["id"]


@step(u'the user verifies that the status code is {status_code:d}')
def step_impl_status(context, status_code):
    """Verify expected status code

    :param context: Global context from behave
    :type context: obj
    :param status_code: status code retrieved
    :type status_code: int
    """
    assert_that(context.status_code).is_equal_to(status_code)


@step(u'verifies response body with')
def step_impl_validate_body(context):
    """Verify response body

    :param context: Global context from behave
    :type context: obj
    """
    body = jira_utils.generate_data(context.table)
    assert_that(json_comparator.contains(context.json_response, body)[0],
                f"Expected that {body} is in {context.json_response}").is_true()


@step(u'verifies the schema with "{schema}"')
def step_impl_verify_schema(context, schema):
    """Verify json response with a schema

    :param context: Global context from behave
    :type context: obj
    :param schema: schema file path
    :type schema: string
    """

    json_schema = JsonManager.read_json('schemas', schema)
    assert_that(validate(instance=context.json_response, schema=json_schema)).is_equal_to(None)


@step(u'Used payload body is "{body}"')
def step_impl_payload(context, body):
    """Load a json file containing payload body for a request

    :param context: Global context from behave
    :type context: obj
    :param body: json body file path
    :type body: string
    """

    context.json_body = json.dumps(JsonManager.read_json('payloads', body))
