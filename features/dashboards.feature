@dashboards
Feature: Dashboards

    @get_all_dashboards @smoke
    Scenario: All dashboards are obtained
        Given the user defines a "GET" request to "/dashboard"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/get_all_dashboards.json"
        And verifies response body with
            | key                              | value             |
            | startAt                          | "0"               |
            | dashboards                       | []                |
            | dashboards.id                    | 10000             |
            | dashboards.isFavourite           | false             |
            | dashboards.name                  | Default dashboard |
            | dashboards.popularity            | "0"               |
            | dashboards.sharePermissions      | []                |
            | dashboards.sharePermissions.id   | "1"               |
            | dashboards.sharePermissions.type | global            |

    @create_dashboard @smoke @fixture.delete.dashboard
    Scenario: A dashboard is created
        Given the user defines a "POST" request to "/dashboard"
            | key              | value                                                     |
            | name             | Dashboard for testing                                     |
            | description      | A dashboard made for testing the creation of a dashboard. |
            | sharePermissions | []                                                        |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/create_dashboard.json"
        And verifies response body with
            | key              | value                                                     |
            | name             | Dashboard for testing                                     |
            | description      | A dashboard made for testing the creation of a dashboard. |
            | sharePermissions | []                                                        |
            | isFavourite      | true                                                      |
            | popularity       | "0"                                                       |

    @get_dashboard @smoke @fixture.create.dashboard @fixture.delete.dashboard
    Scenario: A dashboard is obtained
        Given the user defines a "GET" request to "/dashboard/{dashboardId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/get_dashboard.json"
        And verifies response body with
            | key              | value                                    |
            | name             | Dashboard for testing                    |
            | description      | A dashboard made as a set up for a test. |
            | sharePermissions | []                                       |
            | isFavourite      | true                                     |
            | popularity       | "1"                                      |

    @update_dashboard @smoke @fixture.create.dashboard @fixture.delete.dashboard
    Scenario: A dashboard is updated
        Given the user defines a "PUT" request to "/dashboard/{dashboardId}"
            | key              | value                             |
            | name             | Dashboard name updated            |
            | description      | An updated dashboard description. |
            | sharePermissions | []                                |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/update_dashboard.json"
        And verifies response body with
            | key              | value                             |
            | name             | Dashboard name updated            |
            | description      | An updated dashboard description. |
            | sharePermissions | []                                |
            | isFavourite      | true                              |
            | popularity       | "1"                               |

    @delete_dashboard @smoke @fixture.create.dashboard
    Scenario: A dashboard is deleted
        Given the user defines a "DELETE" request to "/dashboard/{dashboardId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And verifies response body with
            | key | value |
        And the user defines a "GET" request to "/dashboard/{dashboardId}"
        And the user sends the request
        And the user verifies that the status code is 404
        And verifies response body with
            | key    | value |
            | errors | {}    |

    @copy_dashboard @smoke @fixture.create.dashboard @fixture.delete.dashboard @fixture.delete.copied.dashboard
    Scenario: A dashboard is copied
        Given the user defines a "POST" request to "/dashboard/{dashboardId}/copy"
            | key              | value                                |
            | name             | Copied dashboard                     |
            | description      | A copied dashboard made for testing. |
            | sharePermissions | []                                   |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/copy_dashboard.json"
        And verifies response body with
            | key              | value                                |
            | name             | Copied dashboard                     |
            | description      | A copied dashboard made for testing. |
            | sharePermissions | []                                   |
            | isFavourite      | true                                 |
            | popularity       | "0"                                  |

    @search_dashboard @smoke
    Scenario: A dashboard is searched
        Given the user defines a "GET" request to "/dashboard/search?dashboardName=randomName"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "dashboards/search_dashboard.json"
        And verifies response body with
            | key     | value |
            | startAt | "0"   |
            | total   | "0"   |
            | isLast  | true  |
            | values  | []    |
