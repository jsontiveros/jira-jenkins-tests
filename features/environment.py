"""Environment module for behave"""
import json
from behave.model_core import Status
from behave.fixture import use_fixture_by_tag, fixture_call_params
from features.hooks.common_hooks import create_resource, delete_resource
from main.core.requests_manager import RequestsManager
from main.core.utils.decryptor import decryptor


FIXTURE_REGISTRY = {
    "fixture.create.project": fixture_call_params(
        create_resource,
        endpoint="/project",
        body_path="projects/create_project.json"
    ),
    "fixture.delete.project": fixture_call_params(
        delete_resource,
        endpoint="/project/{projectId}"
    ),
    "fixture.create.project.component": fixture_call_params(
        create_resource,
        endpoint="/component",
        body_path="project_components/create_project_component.json"
    ),
    "fixture.delete.project.component": fixture_call_params(
        delete_resource,
        endpoint="/component/{componentId}"
    ),
    "fixture.create.project.role": fixture_call_params(
        create_resource,
        endpoint="/role",
        body_path="project_roles/create_project_role.json"
    ),
    "fixture.delete.project.role": fixture_call_params(
        delete_resource,
        endpoint="/role/{roleId}"
    ),
    "fixture.create.dashboard": fixture_call_params(
        create_resource,
        endpoint="/dashboard",
        body_path="dashboards/create_dashboard.json"
    ),
    "fixture.delete.dashboard": fixture_call_params(
        delete_resource,
        endpoint="/dashboard/{dashboardId}"
    ),
    "fixture.delete.copied.dashboard": fixture_call_params(
        delete_resource,
        endpoint="/dashboard/{copyId}"
    ),
    "fixture.create.issuetype": fixture_call_params(
        create_resource,
        endpoint="/issuetype",
        body_path="issue_types/create_issue_type.json"
    ),
    "fixture.delete.issuetype": fixture_call_params(
        delete_resource,
        endpoint="/issuetype/{issuetypeId}"
    ),
    "fixture.create.issue": fixture_call_params(
        create_resource,
        endpoint="/issue",
        body_path="issues/create_issue.json"
    ),
    "fixture.delete.issue": fixture_call_params(
        delete_resource,
        endpoint="/issue/{issueId}"
    ),
    "fixture.create.issue.typescheme": fixture_call_params(
        create_resource,
        endpoint="/issuetypescheme",
        body_path="issue_type_schemes/create_issue_type_scheme.json",
        id_key="issueTypeSchemeId"
    ),
}


def before_all(context):
    """Before running all features

    :param context: Global context from behave
    :type context: obj
    """
    context.rm = RequestsManager.get_instance()
    context.id_dictionary = {}
    environment_file = context.config.userdata.get("env_file")
    environment = context.config.userdata.get("env")
    environment_data = json.load(open(environment_file))
    context.admin = environment_data[environment]["users"]["admin"]
    user_name = decryptor(context.admin.get("name"))
    token = decryptor(context.admin.get("token"))
    context.base_url = environment_data[environment]["base_url"]
    context.rm.set_credentials(user_name, token)
    context.rm.set_base_url(context.base_url + "/rest/api/2")


def before_scenario(context, scenario):  # pylint: disable=W0613
    """Before running a scenario

    :param context: Global context from behave
    :type context: obj
    :param scenario: scenario model from behave
    :type scenario: obj
    """
    print(f"=============Started {scenario.name}")


def after_scenario(context, scenario):  # pylint: disable=W0613
    """After running a scenario, notify if the scenario failed

    :param context: Global context from behave
    :type context: obj
    :param scenario: scenario model from behave
    :type scenario: obj
    """

    if scenario.status == Status.failed:
        print(f"============ Ooops Failed scenario {scenario.name}")
    print(f"=============Finished {scenario.name}")


def before_feature(context, feature):  # pylint: disable=W0613
    """Before running a feature

    :param context: Global context from behave
    :type context: obj
    :param feature: feature model from behave
    :type feature: obj
    """


def after_feature(context, feature):  # pylint: disable=W0613
    """After running a feature

    :param context: Global context from behave
    :type context: obj
    :param feature: feature model from behave
    :type feature: obj
    """


def before_tag(context, tag):  # pylint: disable=W0613
    """Before running a tag

    :param context: Global context from behave
    :type context: obj
    :param tag: tag asigned to feature or scenario
    :type tag: string
    """
    return use_fixture_by_tag(tag, context, FIXTURE_REGISTRY)\
        if tag.startswith("fixture.create") else None


def after_tag(context, tag):  # pylint: disable=W0613
    """After running a tag

    :param context: Global context from behave
    :type context: obj
    :param tag: tag asigned to feature or scenario
    :type tag: string
    """
    return use_fixture_by_tag(tag, context, FIXTURE_REGISTRY)\
        if tag.startswith("fixture.delete") else None


def after_all(context):
    """After running all features and scenarios

    :param context: Global context from behave
    :type context: obj
    """
    context.rm.close_session()
