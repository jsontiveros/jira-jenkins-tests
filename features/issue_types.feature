@issuetypes
Feature: Issue types

    @get_all_issuetypes @smoke
    Scenario: Get list of all issue types
        Given the user defines a "GET" request to "/issuetype"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issue_types/get_all_issue_types.json"

    @get_issuetype_by_id @smoke @fixture.create.issuetype @fixture.delete.issuetype
    Scenario: Get issue type by id
        Given the user defines a "GET" request to "/issuetype/{issuetypeId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issue_types/get_issue_type.json"

    @get_alternative_issuetypes @smoke @fixture.create.issuetype @fixture.delete.issuetype
    Scenario: Get alternative issue types
        Given the user defines a "GET" request to "/issuetype/{issuetypeId}/alternatives"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issue_types/get_issue_types_alternatives.json"

    @create_issuetype @smoke @fixture.delete.issuetype
    Scenario: Create an IssueType
        Given the user defines a "POST" request to "/issuetype"
            | key         | value                     |
            | name        | Issue type name           |
            | description | An issue type description |
            | type        | standard                  |
        When the user sends the request
        Then the user verifies that the status code is 201
        And verifies the schema with "issue_types/create_issue_type.json"

    @update_issuetype @smoke @fixture.create.issuetype @fixture.delete.issuetype
    Scenario: Update an IssueType
        Given the user defines a "PUT" request to "/issuetype/{issuetypeId}"
            | key         | value                             |
            | name        | Updated issue type name           |
            | description | An updated issue type description |
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issue_types/update_issue_type.json"

    @delete_issuetype @smoke @fixture.create.issuetype
    Scenario: Delete an issue type
        Given the user defines a "DELETE" request to "/issuetype/{issuetypeId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And the user defines a "GET" request to "/issuetype/{issuetypeId}"
        And the user sends the request
        And the user verifies that the status code is 404