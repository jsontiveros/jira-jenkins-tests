@projects
Feature: Projects

    @get_all_projects @smoke
    Scenario: All projects are obtained
        Given the user defines a "GET" request to "/project"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_all_projects.json"

    @get_status_project @smoke @fixture.create.project @fixture.delete.project
    Scenario: All statuses of project are obtained
        Given the user defines a "GET" request to "/project/{projectId}/statuses"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_statuses_project.json"

    @get_issue_hierarchy_project @fixture.create.project @fixture.delete.project
    Scenario: Project issue type hierarchy is obtained
        Given the user defines a "GET" request to "/project/{projectId}/hierarchy"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_issue_hierarchy_project.json"

    @get_notification_scheme_project @fixture.create.project @fixture.delete.project
    Scenario: Project notification scheme is obtained
        Given the user defines a "GET" request to "/project/{projectId}/notificationscheme"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_notification_scheme_project.json"
        And verifies response body with
            | key    | value                                                     |
            | expand | notificationSchemeEvents,user,group,projectRole,field,all |
            | name   | TESS: Simplified Notification Scheme                      |



    @get_project @smoke @fixture.create.project @fixture.delete.project
    Scenario: A project is obtained
        Given the user defines a "GET" request to "/project/{projectId}"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "projects/get_project.json"
        And verifies response body with
            | key            | value                                 |
            | key            | TESS                                  |
            | description    | Example Automation Test to be Removed |
            | assigneeType   | PROJECT_LEAD                          |
            | name           | Automation Test                       |
            | projectTypeKey | software                              |

    @create_project @smoke @fixture.delete.project
    Scenario Outline: A project with template <name> is created
        Given the user defines a "POST" request to "/project"
            | key                | value        |
            | key                | <key>        |
            | name               | <name>       |
            | projectTypeKey     | software     |
            | projectTemplateKey | <template>   |
            | description        | Example      |
            | leadAccountId      | (accountId)  |
            | assigneeType       | PROJECT_LEAD |
        When the user sends the request
        Then the user verifies that the status code is 201
        And verifies the schema with "projects/create_project.json"
        And verifies response body with
            | key | value |
            | key | <key> |

        Examples: Project params
            | key | name           | template                                                |
            | ANB | agility-kanban | com.pyxis.greenhopper.jira:gh-simplified-agility-kanban |
            | ANC | agility-scrum  | com.pyxis.greenhopper.jira:gh-simplified-agility-scrum  |
            | AND | basic          | com.pyxis.greenhopper.jira:gh-simplified-basic          |
            | ANE | kanban-classic | com.pyxis.greenhopper.jira:gh-simplified-kanban-classic |
            | ANF | scrum-classic  | com.pyxis.greenhopper.jira:gh-simplified-scrum-classic  |

    @delete_project @smoke @fixture.create.project
    Scenario: A project is deleted
        Given the user defines a "DELETE" request to "/project/{projectId}"
        When the user sends the request
        Then the user verifies that the status code is 204
        And the user defines a "GET" request to "/project/{projectId}"
        And the user sends the request
        And the user verifies that the status code is 404

    ############## NEGATIVE TEST CASES #####################

    @get_status_project_no_project @negative
    Scenario Outline: All statuses of project without project are not obtained
        Given the user defines a "GET" request to "/project/<idOrKey>/statuses"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                               |
            | errors        | {}                                                  |
            | errorMessages | []                                                  |
            | errorMessages | No project could be found with <param> '<idOrKey>'. |

        Examples: Project ids
            | idOrKey | param |
            | 10030   | id    |
            | TESTT   | key   |

    @get_issue_hierarchy_project_no_project @negative
    Scenario Outline: Project issue type hierarchy without project is not obtained
        Given the user defines a "GET" request to "/project/<id>/hierarchy"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value              |
            | errors        | {}                 |
            | errorMessages | []                 |
            | errorMessages | Project not found. |

        Examples: Project ids
            | id    |
            | 10030 |

    @get_notification_scheme_project_invalid_id @negative
    Scenario Outline: Project notification scheme with invalid id or key is not obtained
        Given the user defines a "GET" request to "/project/<idOrKey>/notificationscheme"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                            |
            | errors        | {}                               |
            | errorMessages | []                               |
            | errorMessages | Selected project does not exist. |

        Examples: Project ids
            | idOrKey |
            | 10030   |
            | TESTT   |

    @get_project_components_paginated_no_project @negative
    Scenario Outline: Project components paginated without project are not obtained
        Given the user defines a "GET" request to "/project/<idOrKey>/component"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                               |
            | errors        | {}                                                  |
            | errorMessages | []                                                  |
            | errorMessages | No project could be found with <param> '<idOrKey>'. |

        Examples: Project ids
            | idOrKey | param |
            | 10030   | id    |
            | TESTT   | key   |

    @get_project_no_id @negative
    Scenario Outline: A project with invalid id or key is not obtained
        Given the user defines a "GET" request to "/project/<idOrKey>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                               |
            | errors        | {}                                                  |
            | errorMessages | []                                                  |
            | errorMessages | No project could be found with <param> '<idOrKey>'. |

        Examples: Project ids
            | idOrKey | param |
            | 10030   | id    |
            | TESTT   | key   |

    @create_project_no_leadid @negative
    Scenario Outline: A project without leadAccountId and invalid template <name> is not created
        Given the user defines a "POST" request to "/project"
            | key                | value        |
            | key                | <key>        |
            | name               | <name>       |
            | projectTypeKey     | software     |
            | projectTemplateKey | <template>   |
            | description        | Example      |
            | assigneeType       | PROJECT_LEAD |
        When the user sends the request
        Then the user verifies that the status code is 400
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                                                                                             |
            | errorMessages | []                                                                                                                |
            | errorMessages | An invalid project template was specified. Make sure the project template matches the project type you specified. |

        Examples: Project params
            | key | name           | template                                                                       |
            | ANB | agility-kanban | com.atlassian.jira-core-project-templates:jira-core-simplified-process-control |

    @delete_project @negative
    Scenario Outline: A project is deleted
        Given the user defines a "DELETE" request to "/project/<idOrKey>"
        When the user sends the request
        Then the user verifies that the status code is 404
        And verifies the schema with "error/error.json"
        And verifies response body with
            | key           | value                                           |
            | errors        | {}                                              |
            | errorMessages | []                                              |
            | errorMessages | No project could be found with key '<idOrKey>'. |

        Examples: Project ids
            | idOrKey | param |
            | 10030   | id    |
            | TESTT   | key   |