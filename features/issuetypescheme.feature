@issues_type_scheme
Feature: Issue Type Scheme

    @get_all_issue_type_schemes @smoke
    Scenario: Get all issue type schemes
        Given the user defines a "GET" request to "/issuetypescheme"
        When the user sends the request
        Then the user verifies that the status code is 200
        And verifies the schema with "issuetypeschemes/get_all_issue_type_schemes.json"


    @delete_issue_type_scheme @smoke @fixture.create.issue.typescheme
    Scenario: An issue type scheme is deleted
        Given the user defines a "DELETE" request to "/issuetypescheme/{issuetypeschemeId}"
        When the user sends the request
        And the user defines a "DELETE" request to "/issuetypescheme/{issuetypeschemeId}"
        Then the user verifies that the status code is 204
        And the user sends the request
        And the user verifies that the status code is 404
