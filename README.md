# Fundación Jala - AT Nationwide Bootcamp

## Automation Testing for Jira Project

Jira Automation Framework Copyright (c) 2021 Jalasoft
2643 Av. Melchor Pérez de Olguín, Colquiri Sud, Cochabamba, Bolivia.
1376 subsuelo Edif. La Unión, Av. Gral. Inofuentes, Calacoto, La Paz, Bolivia
All rights reserved

This software is the confidential and propietary information of
Jalasoft, ("Confidential Information"). You shall not disclose
such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered
into with Jalasoft.

## Install

Execute the basic commands to
Install requirements

`make init`

Enter Environment:

`pipenv shell`

Check Code:

`make check`

Execute Tests

`make test`

