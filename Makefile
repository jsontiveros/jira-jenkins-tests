init:
	pip install -r requirements.txt

check:
	flake8 features/ main/
	pylint features/ main/
	pycodestyle features/ main/

test:
	behave

wip:
	behave -w -k

report:
	behave -f allure_behave.formatter:AllureFormatter -o reports ./features

allure:
	allure serve reports
