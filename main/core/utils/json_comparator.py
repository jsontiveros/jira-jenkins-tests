"""Module for comparing JSON

This code is based on the jsoncompare project developed by Daniel Myers.
Github repository: https://github.com/ChannelIQ/jsoncompare
"""

import json


class Stack:
    """Stack of detected differences between two JSON"""

    def __init__(self):
        """Set values to attributes for a Stack instance"""

        self.stack_items = []

    def append(self, stack_item):
        """Append a stack item

        :param stack_item: an object containing Reason of mismatch,
                            expected and actual JSON
        :type stack_item: StackItem
        """
        self.stack_items.append(stack_item)
        return self

    def __str__(self):
        """Set string representation for a Stack instance"""

        stack_dump = ''
        for item in self.stack_items:
            stack_dump += str(item)
        return stack_dump


class StackItem:
    """Stack item for catching a detected difference between two JSON"""

    def __init__(self, reason, expected, actual):
        """Set values to attributes for a StackItem instance"""

        self.reason = reason
        self.expected = expected
        self.actual = actual

    def __str__(self):
        """Set string representation for a Stack instance"""

        return '\n\nReason: {0}\nExpected:\n{1}\nActual:\n{2}' \
               .format(self.reason, _format_report(self.expected), _format_report(self.actual))


def _indent(multiline):
    """Add identation to every line of a multiline string

    :param multiline: a multiline string
    :type multiline: string
    """
    return '\n'.join('  ' + line for line in multiline.splitlines())


def _format_report(json_obj):
    """Generate a json formated for a report

    :param json_obj: JSON data
    :type json_obj: dictionary
    """
    return _indent(_generate_pprint_json(json_obj))


def _generate_pprint_json(json_obj):
    """Generate indented multiline string of a JSON

    :param json_obj: JSON data
    :type json_obj: dictionary
    """
    return json.dumps(json_obj, sort_keys=True, indent=4)


def _is_dict_same(expected, actual, ignore_value_of_keys, ignore_missing_keys):
    """Determine if two dictionaries have the same values

    :param expected: reference dictionary
    :type expected: dictionary
    :param actual: actual dictionary
    :type actual: dictionary
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
    :type ignore_value_of_keys: list
    :param ignore_missing_keys: flag to indicate to ignore missing keys from 'actual'
    :type ignore_missing_keys: boolean
    """
    for key in expected:
        if key in ignore_value_of_keys:
            continue
        if key in actual:
            are_same_flag, stack = _are_same(expected[key], actual[key], ignore_value_of_keys, ignore_missing_keys)
            if not are_same_flag:
                return False, stack.append(
                    StackItem('Different values', expected[key], actual[key]))
        elif not ignore_missing_keys:
            return False, Stack().append(
                StackItem('Expected key "{0}" Missing from Actual'.format(key), expected, actual))

    return True, Stack()


def _is_list_same(expected, actual, ignore_value_of_keys, ignore_missing_keys):
    """Determine if two lists have the same values

    :param expected: reference list
    :type expected: list
    :param actual: actual list
    :type actual: list
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
                                (if there are dictionaries inside the list)
    :type ignore_value_of_keys: list
    :param ignore_missing_keys: flag to indicate to ignore missing keys from 'actual'
                                (if there are dictionaries inside the list)
    :type ignore_missing_keys: boolean
    """
    for i in range(len(expected)):
        are_same_flag, stack = _are_same(expected[i], actual[i], ignore_value_of_keys, ignore_missing_keys)
        if not are_same_flag:
            return False, stack.append(
                StackItem('Different values (Check order)', expected[i], actual[i]))
    return True, Stack()


def _bottom_up_sort(unsorted_json):
    """Sort key-value pairs in alphabetical descending order in a JSON

    :param unsorted_json: JSON data
    :type unsorted_json: dictionary
    """
    if isinstance(unsorted_json, list):
        new_list = []
        for i in range(len(unsorted_json)):
            new_list.append(_bottom_up_sort(unsorted_json[i]))
        return sorted(new_list)

    elif isinstance(unsorted_json, dict):
        new_dict = {}
        for key in sorted(unsorted_json):
            new_dict[key] = _bottom_up_sort(unsorted_json[key])
        return new_dict

    else:
        return unsorted_json


def _are_same(expected, actual, ignore_value_of_keys, ignore_missing_keys=False):
    """Determine if two ojects have the same values

    :param expected: reference ojects
    :type expected: oject
    :param actual: actual ojects
    :type actual: oject
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
                                (if the object is a dictionary)
    :type ignore_value_of_keys: list
    :param ignore_missing_keys: flag to indicate to ignore missing keys from 'actual'
                                (if the object is a dictionary)
    :type ignore_missing_keys: boolean
    """
    if expected is None:
        return expected == actual, Stack()

    if type(expected) != type(actual):
        return False, Stack().append(
            StackItem('Type Mismatch: Expected Type: {0}, Actual Type: {1}'.format(
                type(expected), type(actual)), expected, actual))

    if type(expected) in (int, str, bool, float):
        return expected == actual, Stack()

    if ignore_missing_keys:
        if len(expected) < len(actual):
            return False, Stack().append(
                StackItem('More data than expected: Maximum Expected Length: {0}, Actual Length: {1}'.format(
                    len(expected), len(actual)), expected, actual))
    else:
        if len(expected) != len(actual):
            return False, Stack().append(
                StackItem('Length Mismatch: Expected Length: {0}, Actual Length: {1}'.format(
                    len(expected), len(actual)), expected, actual))

    if isinstance(expected, dict):
        return _is_dict_same(expected, actual, ignore_value_of_keys, ignore_missing_keys)

    if isinstance(expected, list):
        return _is_list_same(expected, actual, ignore_value_of_keys, ignore_missing_keys)

    return False, Stack().append(StackItem('Unhandled Type: {0}'.format(type(expected)), expected, actual))


def are_same(expected, actual, ignore_list_order_recursively=False, ignore_value_of_keys=[]):
    """Determine if two JSON have the same content

    :param expected: reference JSON
    :type expected: dictionary
    :param actual: actual JSON
    :type actual: dictionary
    :param ignore_list_order_recursively: flag to indicate to ignore the order of JSON data recursively
    :type ignore_list_order_recursively: boolean
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
                                (if the object is a dictionary)
    :type ignore_value_of_keys: list
    """
    if ignore_list_order_recursively:
        expected = _bottom_up_sort(expected)
        actual = _bottom_up_sort(actual)

    return _are_same(expected, actual, ignore_value_of_keys)


def contains(container, content, ignore_list_order_recursively=False, ignore_value_of_keys=[]):
    """Determine if the content of a JSON is included in another JSON

    :param container: reference JSON
    :type container: dictionary
    :param content: JSON content to check in container
    :type content: dictionary
    :param ignore_list_order_recursively: flag to indicate to ignore the order of JSON data recursively
    :type ignore_list_order_recursively: boolean
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
                                (if the object is a dictionary)
    :type ignore_value_of_keys: list
    """
    if ignore_list_order_recursively:
        container = _bottom_up_sort(container)
        content = _bottom_up_sort(content)

    return _are_same(container, content, ignore_value_of_keys, True)


def json_are_same(expected, actual, ignore_list_order_recursively=False, ignore_value_of_keys=[]):
    """Determine if two JSON in string format have the same content

    :param expected: reference JSON
    :type expected: dictionary
    :param actual: JSON content to check in container
    :type actual: dictionary
    :param ignore_list_order_recursively: flag to indicate to ignore the order of JSON data recursively
    :type ignore_list_order_recursively: boolean
    :param ignore_value_of_keys: keys whose values should be ignored in the comparison
                                (if the object is a dictionary)
    :type ignore_value_of_keys: list
    """
    return are_same(json.loads(expected), json.loads(actual), ignore_list_order_recursively, ignore_value_of_keys)
