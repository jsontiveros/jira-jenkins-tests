"""Module for project settings"""
from pathlib import Path


class ProjectDirectories:  # pylint: disable=R0903
    """Class to manage project useful directories"""
    BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent
    RESOURCES_DIR = BASE_DIR / 'main/jira/resources'
    CONFIG_DIR = BASE_DIR / 'config'
