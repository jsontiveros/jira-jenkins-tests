"""Module to request utilitaries"""
import re


class RequestUtils:
    """Class to manage utilitaries"""

    @staticmethod
    def validate_body(body, expected_data):
        # validate body here
        pass

    @staticmethod
    def parse(value):
        """Parse value value literal

        :param value: value to parse
        :type value: string
        """

        if '"' in value:
            return int(value[1:-1])
        parsed_params = {"true": True, "false": False, "None": None, "{}": {}, "[]": []}
        return parsed_params.get(value) if value.lower() in parsed_params else value

    @staticmethod
    def parse_id(id_dict, endpoint):
        """Replace id's to endpoint id references

        :param id_dict: dictionary that contains id's needed on endpoint
        :type id_dict: dictionary
        :param endpoint: application's endpoint
        :type endpoint: string
        """
        id_keys = re.findall(r'{(.*?)}', endpoint)
        for key in id_keys:
            endpoint = re.sub(f'{{({key}*?)}}', str(id_dict[key]), endpoint)
        return endpoint

    @staticmethod
    def get_id_keys(endpoint):
        id_keys = re.findall(r'{(.*?)}', endpoint)
        if not id_keys:
            id_keys.append(endpoint.split('/')[-1] + "Id")

        return id_keys
