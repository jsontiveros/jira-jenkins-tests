""" Module to manage loggers """
import logging
import logging.config


def get_logger(logger):
    logging.config.fileConfig('logger.conf')
    return logging.getLogger(logger)
