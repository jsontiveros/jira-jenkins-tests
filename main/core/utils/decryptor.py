from cryptography.fernet import Fernet
from main.core.utils.settings_utils import ProjectDirectories as dirs


def load_key(path="auth/secret.key"):
    """
    Load the previously generated key

    :param path: path to secret key
    :type path: string
    """
    return open(dirs.RESOURCES_DIR / path, "rb").read()


def decryptor(encrypted):
    """
    Decrypt an encrypted message

    :param encrypted: encrypted message
    :type encrypted: string
    """
    key = load_key()
    algorithm = Fernet(key)
    return algorithm.decrypt(encrypted.encode())
