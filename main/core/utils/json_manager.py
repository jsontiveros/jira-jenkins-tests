"""Module for json related functionalities"""
import json
from main.core.utils.settings_utils import ProjectDirectories as dirs
from main.jira.utils.request_utils import JiraRequestUtils as jira_utils


class JsonManager:
    """Class to manage json data"""

    @staticmethod
    def read_json(path, json_name):
        """Get json data from json file

        :param path: path to json file
        :type path: string
        :param json_name: json file name
        :type json_name: string
        """

        accountId = jira_utils().get_admin_id()

        with open(dirs.RESOURCES_DIR / path / json_name, 'r+') as json_file:
            json_data = json.loads(json_file.read())
            if json_name == 'projects/create_project.json':
                json_data.update({"leadAccountId":  accountId})

        return json_data
