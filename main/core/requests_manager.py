"""Module for requests"""
import json
from http import HTTPStatus
from requests import Session
from requests.auth import HTTPBasicAuth
import main.core.utils.logger.logger_utils as logger

logger = logger.get_logger('basic_logger')


class RequestsManager:
    """Request Manager basic Implementation"""

    __instance = None

    def __init__(self):
        """Set values to attributes for RequestsManager instance"""

        self.base_url = "https://bc03-testing-environment.atlassian.net/rest/api/2"
        self.headers = {"Accept": "application/json", "Content-Type": "application/json"}
        self.session = Session()

    @staticmethod
    def get_instance():
        """Get an instance of the RequestsManager class.

        Returns:
            RequestManager -- return an instance of RequestsManager class.
        """
        if RequestsManager.__instance is None:
            RequestsManager.__instance = RequestsManager()
        return RequestsManager.__instance

    def set_credentials(self, user, token):
        self.auth = HTTPBasicAuth(user, token)

    def set_base_url(self, url):
        self.base_url = url

    def do_request(self, http_method, endpoint, body=None, **kwargs):
        """Send HTTP requests

        :param http_method: HTTP method
        :type http_method: string
        :param endpoint: application's endpoint method
        :type endpoint: string
        :param body: payload body for a request if needed
        :type body: string
        :param kwargs: any other parameter needed to send a request
        :type kwargs: dictionary
        """
        self.auth = kwargs.get("auth", self.auth)
        url = f"{self.base_url}{endpoint}"
        logger.debug('Called Method: %s', http_method)
        logger.debug('To: %s', endpoint)

        if body:
            response = self.session.request(
                http_method, url, headers=self.headers, auth=self.auth, data=body)
        else:
            response = self.session.request(
                http_method, url, headers=self.headers, auth=self.auth)

        if not response.ok:
            return response.status_code, json.loads(response.text)
        if response.status_code is HTTPStatus.NO_CONTENT.value:
            return response.status_code, {}
        return response.status_code, response.json()

    def close_session(self):
        """Close logged in session"""
        self.session.close()
