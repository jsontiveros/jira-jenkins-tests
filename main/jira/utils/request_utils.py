"""Module to request utilitaries for jira"""

from main.core.requests_manager import RequestsManager
from main.core.utils.request_utils import RequestUtils as utils
from main.core.utils.api_constants import HttpMethods as method


class JiraRequestUtils:
    """Class to manage utilitaries for jira"""

    @staticmethod
    def generate_data(table):
        """Generate a parsed dictionary from a table

        :param table: behave Table model
        :type table: obj dashboards.owner.accountId
        """
        data = {}

        if table:
            for row in table:
                keys = row['key'].split('.')
                obj = data
                while keys:
                    key = keys[0]
                    keys.remove(key)
                    value = utils.parse(row['value'])
                    if value == '(accountId)':
                        value = JiraRequestUtils.get_admin_id()
                    value = accountId if value == '(accountId)' else value
                    if key in obj:
                        if isinstance(obj[key], list):
                            if not keys:
                                obj[key].append({} if isinstance(value, list) else value)
                            elif len(obj[key]) == 0:
                                obj[key].append({})
                            obj = obj[key][-1]
                        else:
                            obj = obj[key]
                    else:
                        obj[key] = {} if keys else value
                        obj = obj[key]
        return data

    @staticmethod
    def get_admin_id():

        res_code, res_json = RequestsManager().get_instance().do_request(method.GET.value, '/users/search')

        for keyval in res_json:
            if 'emailAddress' in keyval:
                accountId = keyval['accountId']

        return accountId
